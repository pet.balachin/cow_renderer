package main

import (
	"bufio"
	"cow_renderer/internal"
	"cow_renderer/pkg"
	"io/ioutil"
	"math"
	"os"
	"strings"
)

func main() {

	content, err := ioutil.ReadFile("cow.obj")
	if err != nil {
		panic(err)
	}
	lines := strings.Split(string(content), "\r\n")
	op := internal.NewObjectParser(lines)
	object := op.ToGroup().ToMesh()
	object.Transform(*pkg.NewMatrix().RotateX(math.Pi/2).Scale(2, 2, 2))

	scene_provider := internal.SceneProvider{}
	scene := scene_provider.GetScene()
	scene.Objects = *object

	rt := internal.RayTracer{}
	image := rt.Render(scene)

	f, err := os.Create("mooow.ppm")
	w := bufio.NewWriter(f)
	pkg.ToPpm(image, *w)
}
