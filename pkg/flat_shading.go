package pkg

type FlatShading struct {
}

func (fs *FlatShading) Illuminate(light PointLight, intersection Intersection) Color {
	lightDir := VectorSum(light.Position, VectorMultiplyByNumber(intersection.P, -1))
	color := Dot(intersection.Normal, lightDir)
	color += .2
	return Color{
		R: color,
		G: color,
		B: color,
	}
}
