package pkg

type Canvas struct {
	width  int
	height int
	Canvas [][]Color
}

func NewCanvas(width, height int, color Color) Canvas {
	c := Canvas{
		width:  width,
		height: height,
	}

	c.Canvas = make([][]Color, height)
	for i := range c.Canvas {
		c.Canvas[i] = make([]Color, width)
	}

	for x := 0; x < width; x++ {
		for y := 0; y < height; y++ {
			c.Canvas[y][x] = color
		}
	}

	return c
}
