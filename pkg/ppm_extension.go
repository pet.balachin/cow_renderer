package pkg

import (
	"bufio"
	"math"
)

type PpmExtension struct {
}

var maxLineLength = 70

func ToPpm(canvas Canvas, w bufio.Writer) {
	for y := 0; y < canvas.height; y++ {
		lineLength := 0
		for x := 0; x < canvas.width; x++ {
			color := canvas.Canvas[x][y]
			red := (string)(To256Byte(color.R))
			green := (string)(To256Byte(color.G))
			blue := (string)(To256Byte(color.B))

			if x == 0 {
				w.WriteString(red)
				lineLength += len(red)
			} else {
				lineLength = writeColorComponent(w, maxLineLength, lineLength, red)
			}
			lineLength = writeColorComponent(w, maxLineLength, lineLength, green)
			lineLength = writeColorComponent(w, maxLineLength, lineLength, blue)
		}
	}
	w.Flush()
}

func writeColorComponent(writer bufio.Writer, maxLineLength, lineLength int, red string) int {
	lineLength += 1 + len(red)
	if lineLength > maxLineLength {
		writer.WriteString("")
		lineLength = len(red)
	} else {
		writer.WriteString(" ")
	}
	writer.WriteString(red)

	return lineLength
}

func To256Byte(value float64) byte {
	return (byte)(math.Round(255 * value))
}

//TODO:CLamp
