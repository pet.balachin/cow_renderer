package pkg

import (
	"math"
)

type Matrix struct {
	Identity [][]float64
}

func NewMatrix() *Matrix {
	m := Matrix{Identity: [][]float64{
		{1, 0, 0, 0},
		{0, 1, 0, 0},
		{0, 0, 1, 0},
		{0, 0, 0, 1},
	}}
	return &m
}

func (m *Matrix) translation(x, y, z float64) [][]float64 {
	return [][]float64{
		{1, 0, 0, x},
		{0, 1, 0, y},
		{0, 0, 1, z},
		{0, 0, 0, 1},
	}
}

func (m *Matrix) scaling(x, y, z float64) [][]float64 {
	return [][]float64{
		{x, 0, 0, 0},
		{0, y, 0, 0},
		{0, 0, z, 0},
		{0, 0, 0, 1},
	}
}

func (m *Matrix) rotationX(rad float64) [][]float64 {
	return [][]float64{
		{1, 0, 0, 0},
		{0, math.Cos(rad), -math.Sin(rad), 0},
		{0, math.Sin(rad), math.Cos(rad), 0},
		{0, 0, 0, 1},
	}
}

func (m *Matrix) rotationY(rad float64) [][]float64 {
	return [][]float64{
		{math.Cos(rad), 0, math.Sin(rad), 0},
		{0, 1, 0, 0},
		{-math.Sin(rad), 0, math.Cos(rad), 0},
		{0, 0, 0, 1},
	}
}

func (m *Matrix) rotationZ(rad float64) [][]float64 {
	return [][]float64{
		{math.Cos(rad), -math.Sin(rad), 0, 0},
		{math.Sin(rad), math.Cos(rad), 0, 0},
		{0, 0, 1, 0},
		{0, 0, 0, 1},
	}
}

func (m *Matrix) shearing(xy, xz, yx, yz, zx, zy float64) [][]float64 {
	return [][]float64{
		{1, xy, xz, 0},
		{yx, 1, yz, 0},
		{zx, zy, 1, 0},
		{0, 0, 0, 1},
	}
}

func (m *Matrix) Translate(x, y, z float64) {
	m.Identity = multiply(m.Identity, m.translation(x, y, z))
}

func (m *Matrix) Scale(x, y, z float64) *Matrix {
	m.Identity = multiply(m.Identity, m.scaling(x, y, z))
	return m
}

func (m *Matrix) RotateX(rad float64) *Matrix {
	m.Identity = multiply(m.Identity, m.rotationX(rad))
	return m
}

func (m *Matrix) RotateY(rad float64) *Matrix {
	m.Identity = multiply(m.Identity, m.rotationY(rad))
	return m
}

func (m *Matrix) RotateZ(rad float64) *Matrix {
	m.Identity = multiply(m.Identity, m.rotationZ(rad))
	return m
}

func (m *Matrix) Shear(xy, xz, yx, yz, zx, zy float64) {
	m.Identity = multiply(m.Identity, m.shearing(xy, xz, yx, yz, zx, zy))
}

func multiply(x, y [][]float64) [][]float64 {
	out := make([][]float64, len(y))
	for i := range out {
		out[i] = make([]float64, len(y))
	}
	for i := 0; i < len(x); i++ {
		for j := 0; j < len(y[0]); j++ {
			for k := 0; k < len(y); k++ {
				out[i][j] += x[i][k] * y[k][j]
			}
		}
	}
	return out
}

func Inverse(a [][]float64) Matrix {
	D := Det(a)
	cofm := CofactorMatrix(a)
	inv := multiplyByNumber(cofm, 1/D)
	return Matrix{Identity: inv}

}

func Det(a [][]float64) float64 {
	if len(a) == 1 {
		return a[0][0]
	}
	sign, d := 1, float64(0)
	for i, x := range a[0] {
		d += float64(sign) * x * Det(excludeColumn(a[1:], i))
		sign *= -1
	}
	return d
}

func excludeColumn(a [][]float64, i int) [][]float64 {
	b := make([][]float64, len(a))
	n := len(a[0]) - 1
	for j, row := range a {
		r := make([]float64, n)
		copy(r[:i], row[:i])
		copy(r[i:], row[i+1:])
		b[j] = r
	}
	return b
}

func CofactorMatrix(a [][]float64) [][]float64 {
	cf := make([][]float64, len(a))
	for i := range cf {
		cf[i] = make([]float64, len(a))
	}
	for i := 0; i < len(a); i++ {
		for j := 0; j < len(a); j++ {
			el := GetMinor(a, i, j)
			if el != 0 {
				el = el * math.Pow(-1, float64(i+j))
			}
			cf[j][i] = el
		}
	}
	return cf
}

func GetMinor(a [][]float64, n, m int) float64 {
	minorMtx := make([][]float64, len(a)-1)
	for i := range minorMtx {
		minorMtx[i] = make([]float64, len(a)-1)
	}
	mI, mJ := 0, 0
	for i := 0; i < len(a); i++ {
		for j := 0; j < len(a); j++ {
			if i == n || j == m {
				continue
			}
			minorMtx[mI][mJ] = a[i][j]
			if mJ == len(a)-2 {
				mJ = 0
				mI += 1
			} else {
				mJ += 1
			}
		}
	}
	return Det(minorMtx)
}

func multiplyByNumber(a [][]float64, n float64) [][]float64 {
	for i := 0; i < len(a); i++ {
		for j := 0; j < len(a); j++ {
			if a[j][i] != 0 {
				a[j][i] *= n
			}
		}
	}
	return a
}
