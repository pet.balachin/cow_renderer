package pkg

type Ray struct {
	Origin    Vector
	Direction Vector
}

func (r *Ray) Position(t float64) Vector {
	return VectorSum(r.Origin, VectorMultiplyByNumber(r.Direction, t))
}
