package pkg

type Triangle struct {
	BoxMax Vector
	boxMin Vector

	P1 Vector
	P2 Vector
	P3 Vector
}

func (t *Triangle) Transform(matrix Matrix) {
	t.P1.Transform(matrix)
	t.P2.Transform(matrix)
	t.P3.Transform(matrix)
}

func (t *Triangle) Intersect(r Ray) *Intersection {
	e1 := VectorSum(t.P2, VectorMultiplyByNumber(t.P1, -1))
	e2 := VectorSum(t.P3, VectorMultiplyByNumber(t.P1, -1))

	pvec := Cross(r.Direction, e2)
	det := Dot(e1, pvec)

	if det < 1e-8 && det > -1e-8 {
		return nil
	}
	invDet := 1 / det
	tvec := VectorSum(r.Origin, VectorMultiplyByNumber(t.P1, -1))
	u := Dot(tvec, pvec) * invDet

	if u < 0 || u > 1 {
		return nil
	}

	qvec := Cross(tvec, e1)
	v := Dot(r.Direction, qvec) * invDet

	if v < 0 || u+v > 1 {
		return nil
	}

	f := Dot(e2, qvec) * invDet

	int := Intersection{
		T:      f,
		Normal: Cross(e2, e1),
		P:      r.Position(f),
	}

	return &int
}
