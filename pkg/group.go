package pkg

type Group struct {
	Shapes []Triangle
}

func NewGroup(shapes []Triangle) *Group {
	g := Group{Shapes: shapes}
	return &g
}

func (g *Group) AddShape(shape Triangle) {
	g.Shapes = append(g.Shapes, shape)
}

func (g Group) ToMesh() *Mesh {
	triangles := make([]Triangle, 0)
	for _, shape := range g.Shapes {
		triangles = append(triangles, shape)
	}
	return NewMash(triangles)
}
