package pkg

import (
	//"github.com/kyroy/kdtree"
	"github.com/kyroy/kdtree/points"
)

type Mesh struct {
	triangles []Triangle
	tree      KDTree
}

func NewMash(triangles []Triangle) *Mesh {
	m := Mesh{
		triangles: triangles,
		tree:      *NewKDtree(trianglesToPoints(triangles)),
	}
	return &m
}

func trianglesToPoints(triangles []Triangle) []Point {
	pts := make([]Point, 0)
	for _, triangle := range triangles {
		pts = append(pts, points.NewPoint([]float64{triangle.P1.X, triangle.P2.Y, triangle.P3.Z}, triangle))
	}
	return pts
}

func (m *Mesh) Intersect(r Ray) *Intersection {
	var hitResult *Intersection
	hitResult = nil

	for _, triangle := range m.triangles {
		hit := triangle.Intersect(r)

		if hit == nil {
			continue
		}
		if hitResult == nil {
			hitResult = hit
		} else {
			if hitResult.T < hit.T {
				hitResult = hit
			}
		}

	}
	return hitResult
}

func (m *Mesh) Transform(matrix Matrix) {
	for _, trngl := range m.triangles {
		trngl.Transform(matrix)
	}
}
