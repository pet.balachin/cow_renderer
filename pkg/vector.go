package pkg

import "math"

type Vector struct {
	X float64
	Y float64
	Z float64
}

func (v *Vector) Transform(matrix Matrix) {
	var X1, Y1, Z1 float64
	for j := 0; j < 4; j++ {
		X1 += v.X * matrix.Identity[j][0]
		Y1 += v.Y * matrix.Identity[j][1]
		Z1 += v.Z * matrix.Identity[j][2]
	}
	v.X = X1
	v.Y = Y1
	v.Z = Z1
}

func VectorSum(a Vector, b Vector) Vector {
	return Vector{
		X: a.X + b.X,
		Y: a.Y + b.Y,
		Z: a.Z + b.Z,
	}
}

func VectorMultiplyByNumber(a Vector, n float64) Vector {
	return Vector{
		X: a.X * n,
		Y: a.Y * n,
		Z: a.Z * n,
	}
}
func (v *Vector) Normalize() {
	l := CountLen(*v)
	v.X /= l
	v.Y /= l
	v.Z /= l
}
func CountLen(a Vector) float64 {
	return math.Sqrt(a.X*a.X + a.Y*a.Y + a.Z*a.Z)
}

func Dot(a Vector, b Vector) float64 {
	return a.X*b.X + a.Y*b.Y + a.Z*b.Z
}

func Cross(a Vector, b Vector) Vector {
	return Vector{
		X: (a.Y*b.Z - b.Y*a.Z),
		Y: (a.X*b.Z - b.X*a.Z),
		Z: (a.X*b.Y - a.Y*b.X),
	}
}
