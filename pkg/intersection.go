package pkg

type Intersection struct {
	T      float64
	Normal Vector
	P      Vector
}
