package pkg

var BLACK = Color{1, 1, 1}

type Color struct {
	R float64
	G float64
	B float64
}
