package internal

import (
	"cow_renderer/pkg"
	"strconv"
	"strings"
)

var DEFAULT = pkg.Vector{
	X: 0,
	Y: 0,
	Z: 0,
}

type ObjectParser struct {
	DefaultGroup pkg.Group
	Groups       map[string]pkg.Group

	Vertices []pkg.Vector
	Normals  []pkg.Vector
	Min      pkg.Vector
	Max      pkg.Vector
	Mid      pkg.Vector

	EmptyLinesAmount int
}

func NewObjectParser(lines []string) *ObjectParser {
	op := ObjectParser{
		DefaultGroup:     pkg.Group{},
		Groups:           make(map[string]pkg.Group),
		Vertices:         make([]pkg.Vector, 0),
		Normals:          make([]pkg.Vector, 0),
		Min:              pkg.Vector{},
		Max:              pkg.Vector{},
		Mid:              pkg.Vector{},
		EmptyLinesAmount: 0,
	}

	currentGroup := pkg.Group{Shapes: nil}
	currentGroupName := ""

	for _, line := range lines {
		lineSplit := strings.Split(line, " ")
		if lineSplit != nil {
			switch lineSplit[0] {
			case "v":
				op.parseVertex(lineSplit)
				break
			case "f":
				op.parseFace(lineSplit, &currentGroup, currentGroupName)
				break
			case "g":
				op.parseGroup(lineSplit, &currentGroup, &currentGroupName)
				break
			case "vn":
				op.parseNormal(lineSplit)
				break
			default:
				op.EmptyLinesAmount++
				break
			}
		}
	}
	op.getBounds()
	return &op
}

func (op *ObjectParser) parseVertex(lineSplit []string) {
	if len(lineSplit) == 4 {
		x, errX := strconv.ParseFloat(lineSplit[1], 64)
		y, errY := strconv.ParseFloat(lineSplit[2], 64)
		z, errZ := strconv.ParseFloat(lineSplit[3], 64)

		if errX == nil && errY == nil && errZ == nil {
			op.Vertices = append(op.Vertices, pkg.Vector{
				X: x,
				Y: y,
				Z: z,
			})

			return
		}
	}
	op.EmptyLinesAmount++
}

func (op *ObjectParser) parseFace(lineSplit []string, triangles *pkg.Group, currentGroupName string) {
	if len(lineSplit) >= 4 {
		vertex1 := op.getVertexInfo(lineSplit[1])

		//TODO:Here strange if is {}
		for i := 2; i < len(lineSplit)-1; i++ {
			vertex2 := op.getVertexInfo(lineSplit[i])
			vertex3 := op.getVertexInfo(lineSplit[i+1])

			if vertex2 == DEFAULT && vertex3 == DEFAULT {
				op.EmptyLinesAmount++
				break
			}
			triangles.AddShape(pkg.Triangle{
				P1: vertex1,
				P2: vertex2,
				P3: vertex2,
			})
			op.Groups[currentGroupName] = *triangles
		}

	} else {
		op.EmptyLinesAmount++
	}
}

func (op *ObjectParser) getVertexInfo(txt string) (vertex pkg.Vector) {
	vertex = DEFAULT

	vertexInfo := strings.Split(txt, "/")
	if len(vertexInfo) >= 1 {
		vertexIndex, err := strconv.Atoi(vertexInfo[0])
		if err == nil && vertexIndex >= 1 && vertexIndex <= len(op.Vertices) {
			vertex = op.Vertices[vertexIndex-1]
		}
	}

	return vertex

}

func (op *ObjectParser) parseGroup(lineSplit []string, currentGroup *pkg.Group, currentGroupName *string) {
	if len(lineSplit) == 2 {
		*currentGroupName = lineSplit[1]
		currentGroup = new(pkg.Group)
		op.Groups[*currentGroupName] = *currentGroup
	}
	op.EmptyLinesAmount++
}

func (op *ObjectParser) parseNormal(lineSplit []string) {
	if len(lineSplit) == 4 {
		x, errX := strconv.ParseFloat(lineSplit[1], 64)
		y, errY := strconv.ParseFloat(lineSplit[2], 64)
		z, errZ := strconv.ParseFloat(lineSplit[3], 64)

		if errX == nil && errY == nil && errZ == nil {
			op.Normals = append(op.Normals, pkg.Vector{
				X: x,
				Y: y,
				Z: z,
			})

			return
		}
	}
	op.EmptyLinesAmount++
}

func (op *ObjectParser) getBounds() {
	if len(op.Vertices) == 0 {
		op.Min, op.Max, op.Mid = DEFAULT, DEFAULT, DEFAULT
	}

	minimumX := op.Vertices[0].X
	minimumY := op.Vertices[0].Y
	minimumZ := op.Vertices[0].Z

	maximumX := op.Vertices[0].X
	maximumY := op.Vertices[0].Y
	maximumZ := op.Vertices[0].Z

	for _, vertex := range op.Vertices[1:] {
		if vertex.X < minimumX {
			minimumX = vertex.X
		}
		if vertex.Y < minimumY {
			minimumY = vertex.Y
		}
		if vertex.Z < minimumZ {
			minimumZ = vertex.Z
		}
		if vertex.X > maximumX {
			maximumX = vertex.X
		}
		if vertex.Y > maximumY {
			maximumY = vertex.Y
		}
		if vertex.Z > maximumZ {
			maximumZ = vertex.Z
		}
	}

	op.Min = pkg.Vector{
		X: minimumX,
		Y: minimumY,
		Z: minimumZ,
	}

	op.Max = pkg.Vector{
		X: maximumX,
		Y: maximumY,
		Z: maximumZ,
	}

	op.Mid = pkg.Vector{
		X: (maximumX + minimumX) / 2,
		Y: (maximumY + minimumY) / 2,
		Z: (maximumZ + minimumZ) / 2,
	}

}

func (op *ObjectParser) ToGroup() pkg.Group {
	group := pkg.Group{}
	for _, shape := range op.Groups {
		for _, triangle := range shape.Shapes {
			group.AddShape(triangle)
		}
	}
	return group
}
