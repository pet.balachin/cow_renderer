package internal

import "cow_renderer/pkg"

type SceneProvider struct {
	CameraProvider CameraProvider
	LightProvider  lightProvider
}

func (sp *SceneProvider) GetScene() Scene {

	return Scene{
		Objects: pkg.Mesh{},
		Light:   sp.LightProvider.GetLight(),
		Camera:  sp.CameraProvider.GetCamera(),
	}
}
