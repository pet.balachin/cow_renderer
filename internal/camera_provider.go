package internal

import (
	"math"
)

type CameraProvider struct {
	rayProvider RayProvider
}

func (c *CameraProvider) GetCamera() Camera {
	return NewCamera(300, 300, math.Pi/3, c.rayProvider)
	//TODO:Strange thing
}
