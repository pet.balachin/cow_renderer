package internal

import "cow_renderer/pkg"

type Scene struct {
	Objects pkg.Mesh
	Light   pkg.PointLight
	Camera  Camera
}
