package internal

import "cow_renderer/pkg"

type lightProvider struct {
}

func (lp *lightProvider) GetLight() pkg.PointLight {
	return pkg.PointLight{Position: pkg.Vector{
		X: 0,
		Y: 0,
		Z: 300,
	}}
}
