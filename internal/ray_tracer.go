package internal

import "cow_renderer/pkg"

type RayTracer struct {
	fs pkg.FlatShading
}

func (r *RayTracer) Render(scene Scene) pkg.Canvas {
	width := scene.Camera.Width
	height := scene.Camera.Height

	image := pkg.NewCanvas(width, height, pkg.BLACK)

	for i := 0; i < width; i++ {
		for j := 0; j < height; j++ {
			ray := scene.Camera.RayForPixel(i, j)
			inter := scene.Objects.Intersect(ray)

			if inter != nil {
				image.Canvas[i][j] = r.fs.Illuminate(scene.Light, *inter)
			}
		}
	}
	return image

}
