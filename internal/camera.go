package internal

import (
	"cow_renderer/pkg"
	"math"
)

type Camera struct {
	rayProvider RayProvider
	Angle       float64
	Width       int
	Height      int
	Direction   pkg.Vector
	Origin      pkg.Vector
	Transform   pkg.Matrix
	PixelSize   float64
	HalfWidth   float64
	HalfHeight  float64
	FieldOfView float64
}

func NewCamera(horizontalSize, verticalSize int, fieldOfView float64, rayProvider RayProvider) Camera {
	c := Camera{
		rayProvider: rayProvider,
		Angle:       0,
		Width:       horizontalSize,
		Height:      verticalSize,
		Direction:   pkg.Vector{},
		Origin:      pkg.Vector{},
		Transform:   *pkg.NewMatrix(),
		PixelSize:   0,
		HalfWidth:   0,
		HalfHeight:  0,
		FieldOfView: fieldOfView,
	}

	halfView := math.Tan(fieldOfView / 2)
	aspect := (float64)(horizontalSize / verticalSize)

	if aspect >= 1 {
		c.HalfWidth = halfView
		c.HalfHeight = halfView / aspect
	} else {
		c.HalfWidth = halfView * aspect
		c.HalfHeight = halfView
	}
	c.PixelSize = c.HalfWidth * 2 / (float64)(horizontalSize)

	return c
}

func (c *Camera) RayForPixel(px, py int) pkg.Ray {
	return c.rayProvider.RayForPixel(px, py, *c)
}
