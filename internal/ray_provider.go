package internal

import "cow_renderer/pkg"

type RayProvider struct {
}

func (r *RayProvider) RayForPixel(px, py int, camera Camera) pkg.Ray {
	xOffset := ((float64)(px) + 0.5) * camera.PixelSize
	yOffset := (float64(py) + 0.5) * camera.PixelSize

	worldX := camera.HalfWidth - xOffset
	worldY := camera.HalfHeight - yOffset

	inverseTransform := pkg.Inverse(camera.Transform.Identity)
	pixel := pkg.Vector{
		X: worldX,
		Y: worldY,
		Z: -1,
	}
	pixel.Transform(inverseTransform)
	origin := pkg.Vector{
		X: 0,
		Y: 0,
		Z: 0,
	}
	origin.Transform(inverseTransform)

	direction := pkg.VectorSum(pixel, pkg.VectorMultiplyByNumber(origin, -1))
	direction.Normalize()

	return pkg.Ray{
		Origin:    origin,
		Direction: direction,
	}
}
